import { Component, OnInit } from '@angular/core';
import { RestApiService } from '../shared/rest-api.service';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.css']
})
export class ContactListComponent implements OnInit {

  contacts = [];

  constructor(private restApiService: RestApiService) { }

  ngOnInit() {
    this.loadContacts();
  }

  loadContacts() {
    return this.restApiService.getAll().subscribe( data => {
      this.contacts = data;
      console.log(data);
    });
  }

  delete(id: number) {
    this.restApiService.delete(id).subscribe( data => {
      this.loadContacts();
    });
  }
}
