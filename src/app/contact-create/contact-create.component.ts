import { Component, OnInit, Input } from '@angular/core';
import { RestApiService } from '../shared/rest-api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contact-create',
  templateUrl: './contact-create.component.html',
  styleUrls: ['./contact-create.component.css']
})
export class ContactCreateComponent implements OnInit {

  contact = {};
  constructor(
    private restApiService: RestApiService,
    private router: Router) { }

  ngOnInit() {
  }

  create() {
    this.restApiService.create(this.contact).subscribe(data => {
      this.router.navigate(['/contact-list']);
      console.log('CREATE', data);
    });
  }

}
