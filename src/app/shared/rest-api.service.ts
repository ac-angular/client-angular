import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Contact } from './contact';

@Injectable({
  providedIn: 'root'
})
export class RestApiService {

  // Leer READMI para levantar cada proyecto
  // api = 'http://localhost:3000/contacts'; // WEB-SERVER-FAKE almacenado en este proyecto
  // api = 'http://localhost:8080/api/v1/contacts'; // Proyecto: api-rest-golang. La API-REST tiene control de versiones de URI
  api = 'http://127.0.0.1:8001/api/contacts'; // Proyecto: api-rest-laravel
  constructor(private httpClient: HttpClient) { }

    // Http Options
    httpOptions = {
      headers: new HttpHeaders({
        Accept: 'application/json'
      })
    };

  getAll(): Observable<Contact[]> {
    return this.httpClient.get<Contact[]>(`${this.api}`, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  getById(id): Observable<Contact> {
    return this.httpClient.get<Contact>(this.api + '/' + id, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  update(contact) {
    return this.httpClient.put<Contact>(`${this.api}/${contact.id}`, contact, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  delete(id: number) {
    return this.httpClient.delete<Contact>(`${this.api}/` + id, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  create(contact): Observable<Contact> {
    return this.httpClient.post<Contact>(`${this.api}`, contact, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage =  `Error Client\nMessage: ${error.error.message}`;
    } else {
      // Get server-side error
      errorMessage = `Error Server Side\nCode: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
 }
}
