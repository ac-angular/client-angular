import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RestApiService } from '../shared/rest-api.service';

@Component({
  selector: 'app-contact-edit',
  templateUrl: './contact-edit.component.html',
  styleUrls: ['./contact-edit.component.css']
})
export class ContactEditComponent implements OnInit {
  fieldId = 'id';
  id: number;
  contact = {};

  constructor(
    private activatedRoute: ActivatedRoute,
    private restApiService: RestApiService,
    private router: Router) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.params[this.fieldId];
    this.restApiService.getById(this.id).subscribe(data => {
      this.contact = data;
      console.log('DATA', data);
    });
  }

  update() {
    this.restApiService.update(this.contact).subscribe(data => {
      this.router.navigate(['/contact-list']);
    });
  }

}
