# client-angular

Cliente de la arquitectura cliente-servidor

## Conceptos a desarrollar
Cliente, consumo de API

## Tecnologias
Lenguaje: Typescript

Framework: Angular (versión: 7.3.9)

## Uso

Levantar el servidor de desarrollo al ejecutar el siguiente comando:

```bash
1) ng serve -o
```

## Servidores posibles sobre el cual probar el cliente

### Alt 1) WEB-SERVER-FAKE

Se puede levantar el falso servidor con datos de prueba de la siguiente manera:

```bash
 1) Instalar el servidor `sudo npm install -g json-server`
 2) Ejecutar el servidor `json-server --watch web-service-fake/db.json`
 3) En el archivo rest-api.service.ts establecer en la variable api el valor http://localhost:3000
```

### Alt 2) API-REST-GOLANG

[Proyecto en GitLab](https://gitlab.com/projects-go/api-rest-golang)

```bash
 1) Descargar o clonar el proyecto
 2) En el directorio raiz ejecutar: `go run *.go`
 3) En el archivo rest-api.service.ts establecer en la variable api el valor http://localhost:8080
```
